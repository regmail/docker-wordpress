version: '3.7'
services:
  wordpress:
    #image: wordpress:php7.4-apache
    image: wordpress
    container_name: var_c_name
    restart: always
    environment:
      WORDPRESS_DB_HOST: mariadb
      WORDPRESS_DB_USER: db_user 
      WORDPRESS_DB_PASSWORD: var_db_password 
      WORDPRESS_DB_NAME: wordpress 
    depends_on: 
      - mariadb
    ports:
      - 80      
    networks:
      - web 
      - net-mariadb 
    volumes:
      - "/etc/timezone:/etc/timezone:ro" # meme timezone que l hote
      - "/etc/localtime:/etc/localtime:ro"    
      - ./volumes/wordpress:/var/www/html  
    labels:
      - "traefik.enable=true" 
      - "traefik.http.routers.wp-var_c_name.rule=Host(`var_domaine`)" 
      - "traefik.http.routers.wp-var_c_name.entrypoints=websecure"      
  mariadb:
    container_name: var_c_db_name 
    image: mariadb:10
    restart: always
    ports:
      - 3306
    environment:
      #- ALLOW_EMPTY_PASSWORD=yes
      - MARIADB_ROOT_PASSWORD=generatePassword
      - MARIADB_USER=db_user
      - MARIADB_PASSWORD=var_db_password
      - MARIADB_DATABASE=wordpress
    volumes:
      - ./volumes/mariadb/:/var/lib/mysql
      - "/etc/timezone:/etc/timezone:ro" # meme timezone que l hote
      - "/etc/localtime:/etc/localtime:ro"
    networks:
      - net-mariadb
networks:
  web:
    external: true
  net-mariadb: 
    external: true  
