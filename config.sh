#!/usr/bin/env bash

cp ./docker-compose.yml.org ./docker-compose.yml

#echo "Introduire le nom de domaine (ex : wordpress.domaine.com) :"
read -p "Introduire le nom de domaine [wordpress.linregs.be] : " var_domaine 
var_domaine=${var_domaine:-wordpress.linregs.be}

#echo "Introduire le nom du container wordpress (ex : c_wordpress) :"
read -p "Introduire le nom du container wordpress [c_wordpress] : " var_c_name
var_c_name=${var_c_name:-c_wordpress}

#echo "Introduire le nom du container Mariadb (ex : c_wordpress_db) :"
read -p "Introduire le nom du container Mariadb [c_wordpress_db] : " var_c_db_name
var_c_db_name=${var_c_db_name:-c_wordpress_db}

##echo "Introduire l'email de contact (ex : admin.linux@domaine.be :"
#read -p "Introduire l'email de contact [admin.linux@domaine.be] : " var_email
#var_email=${var_email:-admin.linux@domaine.be}

function generatePassword() {
    openssl rand -hex 16
}

var_db_password=$(generatePassword)
var_MARIADB_ROOT_PASSWORD=$(generatePassword)
MARIADB_PASSWORD=$(generatePassword)

sed -i.`date +\%G\%m\%d-\%Hh\%Mm\%Ss`.bak \
    -e "s/var_c_name/${var_c_name}/g" \
    -e "s/var_domaine/${var_domaine}/g" \
    -e "s/var_c_db_name/${var_c_db_name}/g" \
    -e "s/var_db_password/${var_db_password}/g" \
    -e "s/var_email/${var_email}/g" \
    -e "s#APP_URL=.*#APP_URL=https://${var_domaine}#g" \
    -e "s/var_wordpress_url/${domaine}/g" \
    -e "s#MARIADB_ROOT_PASSWORD=.*#MARIADB_ROOT_PASSWORD=${var_MARIADB_ROOT_PASSWORD}#g" \
    "$(dirname "$0")/docker-compose.yml"

#    -e "s#DB_PASSWORD=.*#DB_PASSWORD=${var_db_password}#g" \
#    -e "s#MARIADB_PASSWORD=.*#MARIADB_PASSWORD=${var_db_password}#g" \
echo "Le fichier docker-compose.yml a été généré"
chmod 640 ./docker-compose.yml
