# Wordpress

WordPress est un système de gestion de contenu gratuit, libre et open-source. Ce logiciel écrit en PHP repose sur une base de données MySQL et est distribué par la fondation WordPress.org. Wikipédia


# Preregis


Utiliser traefik


```
git clone git@gitlab.com:regmail/docker-traefik.git

```

# Configuration


Lancer le script config.sh et répondre aux questions posées

```
./config.sh
```

Remarque : Les valeurs proposées entre [] sont les valeurs prise par default si on fait enter.


# Lancer la stack dokuwiki

Cette stack vous permettras de lancer un container wordpress et un container mariadb.


``` 
docker-compose up -d 
```

Pour voir les logs

```
docker-compose logs -f
```

## Terminer l'installation

Le reste de la configuration se réalise en se rendant sur l'url choisie pour le site.

* Choisir la langue
* Titre du site
* Identifiant (l'admin du site)
* Mot de passe (strong es préférable) sinon si mot de passe faible (pour les tests) cocher la confirmation du mot de passe faible.
* Email de contact
* Installer Wordpress (Prend quelques secondes pour afficher Quel succès !)



## Liens
### Wordpress
#### Officiel 
https://hub.docker.com/_/wordpress
https://github.com/docker-library/wordpress/issues
#### Subfolder
https://stackoverflow.com/questions/58466606/can-t-access-admin-when-wordpress-is-in-a-subfolder
https://stackoverflow.com/questions/40236713/wordpress-nginx-with-docker-static-files-not-loaded
https://www.reddit.com/r/Traefik/comments/csmojv/routing_with_wordpress/extrwc9/
